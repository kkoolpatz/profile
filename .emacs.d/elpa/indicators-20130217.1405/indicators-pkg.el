(define-package "indicators" "20130217.1405" "Display the buffer relative location of line in the fringe." 'nil :url "https://github.com/Fuco1/indicators.el" :keywords '("fringe" "frames"))
