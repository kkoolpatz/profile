#+Title: Emacs Configuration file          
#+Author: Patanjali Hardikar
# -*- mode: org; -*-
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/htmlize.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/readtheorg.css"/>
#+HTML_HEAD: <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
#+HTML_HEAD: <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/lib/js/jquery.stickytableheaders.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/readtheorg/js/readtheorg.js"></script>
* General Emacs Startup Config
** Disable startup screen
#+BEGIN_SRC emacs-lisp
          
(setq inhibit-splash-screen t)
(setq inhibit-startup-message t)
#+END_SRC

** Start Emacs server 
server-start will allow other apps to attach files to existing emacs frame
#+Begin_SRC emacs-lisp
(server-start)
#+END_SRC

** Load custom theme

#+BEGIN_SRC emacs-lisp
(load-theme 'patz-leuven t)
#+END_SRC

** Set Font to Consolas-13pt
#+BEGIN_SRC emacs-lisp
(set-face-attribute 'default nil :font "Consolas-13")
#+END_SRC

** Enable line numbers
#+BEGIN_SRC emacs-lisp
          (global-linum-mode)
#+END_SRC 

** Enable column number in mode line
#+BEGIN_SRC emacs-lisp
          (column-number-mode)
#+END_SRC

** Enable highlight line mode

#+BEGIN_SRC emacs-lisp
;; Disabled as it intereferes with line spacing. 
 (global-hl-line-mode)
#+END_SRC

** Increase Line spacing

#+BEGIN_SRC emacs-lisp
;; Disable if it intereferes with line spacing. 
 (setq line-spacing '0.25 )
#+END_SRC

** Customise Org-bullets

#+BEGIN_SRC emacs-lisp
;; set up unicode
(prefer-coding-system       'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)                      
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

;; use org-bullets-mode for utf8 symbols as org bullets
(require 'org-bullets)
;; make available "org-bullet-face" such that I can control the font size individually
(setq org-bullets-face-name (quote org-bullet-face))
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
(add-hook 'org-mode-hook (lambda () (org-indent-mode  1)))
#+END_SRC

* Keybindings
** Close (kill) current buffer
#+BEGIN_SRC  emacs-lisp
(global-set-key [f4] 'kill-buffer-and-window)
#+END_SRC

** Mode Enabling keys
Enable CUA mode
#+BEGIN_SRC emacs-lisp
(global-set-key [f5] 'cua-mode)
#+END_SRC          

** Customize face
Customize-face so it tells you the current face
#+BEGIN_SRC emacs-lisp
(global-set-key [f6] 'customize-face)
#+END_SRC          

* Functions
** Move Text up / down
Move-text-internal is a helper function that helps move text, either line or selected region, up or down.
This is typically mapped to M-<up> and M-<down> keys

#+BEGIN_SRC emacs-lisp

(defun move-text-internal (arg)
  (cond
   ((and mark-active transient-mark-mode)
    (if (> (point) (mark))
        (exchange-point-and-mark))
    (let ((column (current-column))
          (text (delete-and-extract-region (point) (mark))))
      (forward-line arg)
      (move-to-column column t)
      (set-mark (point))
      (insert text)
      (exchange-point-and-mark)
      (setq deactivate-mark nil)))
   (t
    (let ((column (current-column)))
      (beginning-of-line)
      (when (or (> arg 0) (not (bobp)))
        (forward-line)
        (when (or (< arg 0) (not (eobp)))
          (transpose-lines arg)
          (when (and (eval-when-compile
                       '(and (>= emacs-major-version 24)
                             (>= emacs-minor-version 3)))
                     (< arg 0))
            (forward-line -1)))
        (forward-line -1))
      (move-to-column column t)))))

(defun move-text-down (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines down."
  (interactive "*p")
  (move-text-internal arg))

(defun move-text-up (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines up."
  (interactive "*p")
  (move-text-internal (- arg)))(defun move-text-internal (arg)
  (cond
   ((and mark-active transient-mark-mode)
    (if (> (point) (mark))
        (exchange-point-and-mark))
    (let ((column (current-column))
          (text (delete-and-extract-region (point) (mark))))
      (forward-line arg)
      (move-to-column column t)
      (set-mark (point))
      (insert text)
      (exchange-point-and-mark)
      (setq deactivate-mark nil)))
   (t
    (let ((column (current-column)))
      (beginning-of-line)
      (when (or (> arg 0) (not (bobp)))
        (forward-line)
        (when (or (< arg 0) (not (eobp)))
          (transpose-lines arg)
          (when (and (eval-when-compile
                       '(and (>= emacs-major-version 24)
                             (>= emacs-minor-version 3)))
                     (< arg 0))
            (forward-line -1)))
        (forward-line -1))
      (move-to-column column t)))))

(defun move-text-down (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines down."
  (interactive "*p")
  (move-text-internal arg))

(defun move-text-up (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines up."
  (interactive "*p")
  (move-text-internal (- arg)))


(global-set-key [M-up] 'move-text-up)

(global-set-key [M-down] 'move-text-down)

#+END_SRC

* Org-mode Specific Syntax, functions and key bindings
** Key Bindings for org-mode

[[http://stackoverflow.com/questions/25161792/emacs-org-mode-how-can-i-fold-everything-but-the-current-headline][Org-mode Fold Everything except current headline]]

#+BEGIN_SRC emacs-lisp

(defun org-show-current-heading-tidily ()
  (interactive)  ;Inteactive
  "Show next entry, keeping other entries closed."
  (if (save-excursion (end-of-line) (outline-invisible-p))
      (progn (org-show-entry) (show-children))
    (outline-back-to-heading)
    (unless (and (bolp) (org-on-heading-p))
      (org-up-heading-safe)
      (hide-subtree)
      (error "Boundary reached"))
    (org-overview)
    (org-reveal t)
    (org-show-entry)
    (show-children)))

(global-set-key "\M-=" 'org-show-current-heading-tidily)

#+END_SRC

