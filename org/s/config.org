#+Title: Python Iterators, Iterables and alike
#+Author: Shantanu N. Kulkarni
# -*- mode: org; -*-
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/htmlize.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/readtheorg.css"/>
#+HTML_HEAD: <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
#+HTML_HEAD: <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/lib/js/jquery.stickytableheaders.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/readtheorg/js/readtheorg.js"></script>

* Iterators

- is an object having a object.next() method which upon each call returns 
	- next item of iterator
	- StopIteration exception
- most iterators are built by call to iter(), an builtin function
- the itertools module is the best way to build iterators

---

* Generators

- is simply a function which has one or more yield keywords
- when you call the generator, it does not execute
	- it returns an iterator object
- upon each call to the generator the function execution is frozen
	- with the current point of execution and local variables intact
	- then the expression after the yield is returned
- when the iterator reaches the end or a return is encountered, StopIteration is raised

---

* Generators vs. regular functions

- generators perform "lazy evaluation"
	- i.e. the next value is calculated as and when required
- in contrast, functions compute everything in advance
	- chances are that lots of memory may be consumed
- in case to make the generator also do the same as function 

	=g = list(generator_object())=

---


