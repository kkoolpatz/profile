### File I/O
        
	>>> FH = open('data','r')
	>>> FH # OUT: <open file 'data', mode 'r' at 0x7faf564a49c0>
	>>> FH.readline() # OUT: 'one\n'
	>>> FH.tell() # OUT: 4
	>>> FH.seek(3)
	>>> FH.seek(2)
	>>> FH.readline() # OUT: 'e\n'
	>>> FH.readlines() # OUT: ['two\n', 'three\n']
	>>> FH.close()
	>>> FL = open('newdata','w')
	>>> FL.write('shantanu\n')
	>>> FL.writelines(['foo\n','bar\n'])
	>>> FL.flush()
	>>> FL.close()

### Pickling

	>>> import pickle
	>>> d = {'foo':'zoo', 'bar':'car'}
	>>> p1 = pickle.dumps(d)
	>>> p1 # OUT: "(dp0\nS'foo'\np1\nS'zoo'\np2\nsS'bar'\np3\nS'car'\np4\ns."
	>>> p2 = pickle.loads(p1)
	>>> p2 # OUT: {'foo': 'zoo', 'bar': 'car'}
	>>> FL = open('newdata','w')
	>>> pickle.dump(d,FL)
	>>> pickle.dump('something',FL)
	>>> FL.close()
	>>> FL = open('newdata','r')
	>>> FL.readline() # OUT: '(dp0\n'
	>>> FL.seek(0)
	>>> pickle.load(FL) # OUT: {'foo': 'zoo', 'bar': 'car'}
	>>> pickle.load(FL) # OUT: 'something'
