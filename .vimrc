" Authors: Patanjali S. Hardikar <patanjali@hardikar.in>
" Description: A minimal, but feature rich, example .vimrc. If you are a
"              newbie, basing your first .vimrc on this file is a good choice.
"              If you're a more advanced user, building your own .vimrc based
"              on this file is still a good idea.
"------------------------------------------------------------
" Features {{{1
"
" These options and commands enable some very useful features in Vim, that
" no user should have to live without.

" Set 'nocompatible' to ward off unexpected things that your distro might
" have made, as well as sanely reset options when re-sourcing .vimrc
set nocompatible
 
" Attempt to determine contents. Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
filetype indent plugin on
 
" Enable syntax highlighting
syntax on
 
"------------------------------------------------------------
" Must have options {{{1
"
" These are highly recommended options.
 
" Vim with default settings does not allow easy switching between multiple files
" in the same editor window. Users can use multiple split windows or multiple
" tab pages to edit multiple files, but it is still best to enable an option to
" allow easier switching between files.
"
" One such option is the 'hidden' option, which allows you to re-use the same
" window and switch from an unsaved buffer without saving it first. Also allows
" you to keep an undo history for multiple files when re-using the same window
" in this way. Note that using persistent undo also lets you undo in multiple
" files even in the same window, but is less efficient and is actually designed
" for keeping undo history after closing Vim entirely. Vim will complain if you
" try to quit without saving, and swap files will keep you safe if your computer
" crashes.
set hidden
 
" Note that not everyone likes working this way (with the hidden option).
" Alternatives include using tabs or split windows instead of re-using the same
" window as mentioned above, and/or either of the following options:
" set confirm
" set autowriteall
 
" Better command-line completion
set wildmenu
 
" Show partial commands in the last line of the screen
set showcmd
 
" Highlight searches (use <C-L> to temporarily turn off highlighting; see the
" mapping of <C-L> below)
set hlsearch
 
" Modelines have historically been a source of security vulnerabilities. As
" such, it may be a good idea to disable them and use the securemodelines
" script, <http://www.vim.org/scripts/script.php?script_id=1876>.
" set nomodeline
 
 
"------------------------------------------------------------
" Usability options {{{1
"
" These are options that users frequently set in their .vimrc. Some of them
" change Vim's behaviour in ways which deviate from the true Vi way, but
" which are considered to add usability. Which, if any, of these options to
" use is very much a personal preference, but they are harmless.
 
" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase
 
" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start
 
" When opening a new line and no filetype-specific indenting is enabled, keep
" the same indent as the line you're currently on. Useful for READMEs, etc.
set autoindent
 
" Stop certain movements from always going to the first character of a line.
" While this behaviour deviates from that of Vi, it does what most users
" coming from other editors would expect.
set nostartofline

" Display the cursor position on the last line of the screen or in the status
" line of a window
set ruler
 
" Always display the status line, even if only one window is displayed
set laststatus=2
 
" Instead of failing a command because of unsaved changes, instead raise a
" dialogue asking if you wish to save changed files.
set confirm
 
" Use visual bell instead of beeping when doing something wrong
set visualbell
 
" And reset the terminal code for the visual bell. If visualbell is set, and
" this line is also included, vim will neither flash nor beep. If visualbell
" is unset, this does nothing.
set t_vb=


" Enable 256 colors
set t_Co=256
 
" Enable use of the mouse for all modes
set mouse=a
 
" Set the command window height to 2 lines, to avoid many cases of having to
" "press <Enter> to continue"
set cmdheight=2
 
" Display line numbers on the left
set number
 
" Quickly time out on keycodes, but never time out on mappings
set notimeout ttimeout ttimeoutlen=200
 
" Use <F11> to toggle between 'paste' and 'nopaste'
set pastetoggle=<F11>
 
 
"------------------------------------------------------------
" Indentation options {{{1
"
" Indentation settings according to personal preference.
 
" Indentation settings for using 4 spaces instead of tabs.
" Do not change 'tabstop' from its default value of 8 with this setup.
set shiftwidth=4
set softtabstop=4
set expandtab
 
" Always show VIM Tabs
set showtabline=2 

" Indentation settings for using hard tabs for indent. Display tabs as
" four characters wide.
"set shiftwidth=4
"set tabstop=4
 
 
"------------------------------------------------------------
" Mappings {{{1
"
" Useful mappings
 
" Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
" which is the default
map Y y$
 
" Map <C-L> (redraw screen) to also turn off search highlighting until the
" next search
" nnoremap <C-L> :nohl<CR><C-L>
 
" Maps tabs to Ctrl keys
noremap <C-Left> <Esc>:tabprev<CR>
noremap <C-Right> <Esc>:tabnext<CR>
noremap <C-n> <Esc>:tabnew<enter>

" Another way to preserve selection
nnoremap gV `[v`]

noremap <C-a> gg0VG
noremap <C-A> gg0VG
noremap <C-C> "+y
noremap <C-c> "+y
noremap <C-V> "+p

" Move rows UP and DOWN
nnoremap <C-S-Up> ddkPgV
nnoremap <C-S-Down> ddpgV

vnoremap <C-S-Up> dkPgV
vnoremap <C-S-Up> dkPgV

" Auto text for Todo
setlocal formatoptions=ctnqro
setlocal comments+=n:*,n:#

"make < > shifts keep selection
" Is this really Needed? 
" vnoremap < <gv
" vnoremap > >gv

" Set unix EOL
set fileformat=unix
set fileformats=unix

" Switch between Unix and Dos file formats
fu! FFU()
    execute 'e ++ff=unix'
endfunction

fu! FFD()
    execute 'e ++ff=dos'
endfunction

" Set Speacebar as Defaul Fold
noremap <Space> za

" Set Curson Line
set cursorline

" Stick with the UTF-8 encoding.
if has('multi_byte')
  " Encoding used for the terminal.
    if empty(&termencoding)
        let &termencoding = &encoding
    endif
 
  " Encoding used in buffers, registers, strings in
  " expressions, "viminfo"
  " file, etc.
    set encoding=utf-8

  " Encoding used for writing files.
    setglobal fileencoding=utf-8
endif

colorscheme mustang

fu! Cd2home()
    execute 'cd ~'
endfunction

fu! SaveSess()
    execute 'mksession! ~/.session.vim'
endfunction

fu! RestoreSess()
    "execute '!dos2unix -f ~/.session.vim'
    execute 'so ~/.session.vim'
    if bufexists(1)
        for l in range(1, bufnr('$'))
            if bufwinnr(l) == -1
                exec 'sbuffer ' . l
            endif
        endfor
    endif
syntax on
endfunction


autocmd VimLeave * call SaveSess()
if argc() == 0
autocmd VimEnter * call RestoreSess()
endif
" autocmd VimEnter * call Cd2home()
" autocmd CursorHold,CursorHoldI * update

""""""""""""""""""""""""""
" Macros for manual exec
""""""""""""""""""""""""""

" Clear Jil comments
fu! Clrc()
    execute 'g/\/\*.* /-d'
    execute 'g/\/\*.*/d'
endfunction


" Create Jil comments
fu! Crc()
    execute '%s!\( *\)\([^ ]*_job: \)\([^ ]*\).*!\1/\* ----------------- \3 ----------------- */\r\r\1\2\3!'
endfunction


" Python power line setup

python from powerline.vim import setup as powerline_setup
python powerline_setup()
python del powerline_setup


