;; This file is automatically generated by the multiple-cursors extension.
;; It keeps track of your preferences for running commands with multiple cursors.

(setq mc/cmds-to-run-for-all
      '(
	delete-horizontal-space
	evil-append-line
	evil-backward-char
	evil-copy-from-above
	evil-delete
	evil-delete-char
	evil-end-of-line
	evil-forward-char
	evil-insert-line
	evil-normal-state
	evil-paste-after
	evil-paste-pop
	evil-scroll-down
	evil-scroll-line-down
	evil-scroll-page-down
	evil-search-previous
	kill-region
	org-beginning-of-line
	org-delete-char
	org-end-of-line
	org-force-self-insert
	org-kill-line
	org-return-indent
	org-self-insert-command
	org-shiftright
	org-yank
	))

(setq mc/cmds-to-run-once
      '(
	end-of-buffer
	evil-force-normal-state
	evil-insert
	evil-mouse-drag-region
	evil-next-line
	evil-paste-last-insertion
	evil-previous-line
	evil-yank
	mouse-drag-secondary
	mouse-set-region
	org-insert-heading
	org-shiftleft
	scroll-bar-toolkit-scroll
	))
