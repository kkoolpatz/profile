# -*- mode: org; -*-
#+INCLUDE: "~/org/includes.org"
#+TITLE: git tutorial at [[http://try.github.io][try.github.io]]
#+Author: Patanjali Hardikar

* Basic git commands


   - Start an empty repo
        
        =$ git init=

   - Show the  current state of our project

        =$ git status=

   - Add files to Staging area

        =$ - git add=

   - Git reset to unstage recently added files.

        =$ git reset filename=
        
   - Commit changes to repo

        =$ git commit -m message=

   - View git's Log

        =$ git log=

   - *Google what git remote add origin does*

        =$ git remote add origin https://github.com/try-git/try_git.git=

   - The push command tells Git where to put our commits
   
        =$ git push -u origin master=

   - Pull changes from the git repo
   
        =$ git pull origin master=
        
   - diff what changes
   
        =$ git diff HEAD=
        
   - Revert back to last commit
   
        =$ git checkout -- octocat.txt=

   - Create a new branch named clean_up
   
        =$ git branch clean_up=
        
   - list all branches
   
        =$ git branch=

   - switch git branch to clean_up
     
	=$ git checkout clean_up=

   - remove files and stage for removal in git

	=git rm *.txt=

   - merge clean_up brach with current branch or master

	=git mergo clean_up=

   - delete git branch

	=git branch -d clean_up=

   - 
