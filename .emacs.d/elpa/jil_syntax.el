
(require 'generic-x)

(define-generic-mode 
  'jil-mode                          ;; name of the mode
  '("/*")                           ;; comments delimiter
  '("insert_job" "update_job" "delete_job" "delete_box" "override_job" "metajob" "job_type" "box_name" "command" "owner" "machine" "permission" "condition" "n_retrys" "date_conditions" "days_of_week" "run_calendar" "exclude_calendar" "start_times" "start_mins" "run_window" "description" "term_run_time" "box_terminator" "job_terminator" "std_in_file" "std_out_file" "std_err_file" "watch_file" "watch_file_min_size" "watch_interval" "min_run_alarm" "max_run_alarm" "alarm_if_fail" "profile" "job_load" "priority" "auto_delete" "max_exit_success" "box_success" "box_failure" "timezone" "group" "application")      ;; some keywords
;;  '(("IMREG" . 'Jil-condition-success))
  '(
    ("=" . 'jil-operator) 
    ("&" . 'jil-operator)     ;; some operators
    ("|" . 'jil-operator)
    ("s([^)]*)"        . 'jil-condition-success)
    ("n([^)]*)"        . 'jil-condition-notwith)
    ("[ft]([^)]*)"     . 'jil-condition-failure)
    ("[v]([^)]*)"      . 'jil-condition-variable)


    )     ;; A built-in 
  '("\\.jil$")                    ;; files that trigger this mode
   nil                              ;; any other functions to call
  "My custom highlighting mode"     ;; doc string
)

   (defface jil-condition-success  '((t (:foreground "#006600")))"Green")
   (defface jil-condition-failure  '((t (:foreground "#ff0000")))"Red")
   (defface jil-condition-notwith  '((t (:foreground "#ffff00")))"Yellow")
   (defface jil-condition-variable '((t (:foreground "#0066ff")))"#0066ff")
   (defface jil-operator '((t (:foreground "#777700")))"Dark Yellow")
    
     ;;; c3-mode 
    (define-derived-mode jil-keywords 
     "jil-mode" 
     "A variant of SGML mode. Numbers appear in red."
    
     (font-lock-add-keywords nil '(("[0-9]" . 'jil-keywords)))
    )
