(define-package "org-download" "20160620.607" "Image drag-and-drop for Emacs org-mode" '((async "1.2")) :url "https://github.com/abo-abo/org-download" :keywords '("images" "screenshots" "download"))
