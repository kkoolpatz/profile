;; (set-face-attribute 'default nil :font "Consolas-11")

(require 'cl)
(require 'package) ;; Package manager - All packages load here
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"    ))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"        ))

(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
 (package-initialize) ;; END

(setq inhibit-splash-screen t)
(setq inhibit-startup-message t)

(server-start)

(load-theme 'patz-leuven t)
;;(load-theme 'warm-night t)
;;(load-theme 'patz t)

(custom-set-faces
 '(org-level-1 ((t (:height 2.0 :family "Noto Serif" ))))
 '(org-level-2 ((t (:height 1.7 :family "Noto Serif" ))))
 '(org-level-3 ((t (:height 1.5 :family "Noto Serif" ))))
 '(org-level-4 ((t (:height 1.3 :family "Noto Serif" ))))
 '(org-document-title ((t (:height 2.2 :family "Noto Serif")))))

(show-paren-mode)

(global-linum-mode)

(column-number-mode)

;; Disabled as it intereferes with line spacing. 
;; (global-hl-line-mode nil)

;; Disable if it intereferes with line spacing. 
(add-hook 'org-mode-hook (lambda () (setq line-spacing '0.25 )))

;; Answering just 'y' or 'n' will do
(defalias 'yes-or-no-p 'y-or-n-p)

;; UTF-8 please
(setq locale-coding-system 'utf-8) ; pretty
(set-terminal-coding-system 'utf-8) ; pretty
(set-keyboard-coding-system 'utf-8) ; pretty
(set-selection-coding-system 'utf-8) ; please
(prefer-coding-system 'utf-8) ; with sugar on top

;; Save minibuffer history
(savehist-mode 1)
(setq history-length 1000)

;;(define-globalized-minor-mode my-global-evil-mode evil-mode (lambda () (evil-mode 1)))
;;(my-global-evil-mode 1)

(define-globalized-minor-mode my-global-rainbow-mode rainbow-mode (lambda () (rainbow-mode 1)))
(my-global-rainbow-mode 1)

(define-globalized-minor-mode my-global-yahoo-weather-mode yahoo-weather-mode (lambda () (yahoo-weather-mode 1)))
;;(my-global-yahoo-weather-mode 1)

;; (setq sml/theme 'dark)
;; (setq sml/theme 'light)
;; (setq sml/theme 'respectful)
(setq sml/theme nil)

 (sml/setup)

(global-set-key [f4] 'kill-buffer-and-window)

(global-set-key [f5] 'cua-mode)

(global-set-key [f6] 'customize-face)

(global-set-key "\C-c\C-d" "\C-a\C-k\C-k\C-y\C-y\C-p")

(global-set-key "\C-c\C-m" '(lambda() (interactive) (minimap-mode)))

(global-set-key [M-insert] '(lambda() (interactive) (load-file "~/.emacs")))

(global-set-key (kbd "C-S-c")         'mc/edit-lines                 )
(global-set-key (kbd "C->")           'mc/mark-next-like-this        )
(global-set-key (kbd "C-<" )          'mc/unmark-next-like-this      )
(global-set-key (kbd "C-M-<")         'mc/mark-previous-like-this    )
(global-set-key (kbd "C-M->" )        'mc/unmark-previous-like-this  )
(global-set-key (kbd "C-c C->")       'mc/mark-all-like-this         )
(global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click        )
(global-set-key (kbd "C-<return>")    'set-rectangular-region-anchor )
(global-set-key (kbd "C-M-r" )        'mc/mark-all-in-region         )
(global-set-key (kbd "C-M-S-R" )      'mc/mark-all-in-region-regexp  )

;; (global-set-key (kbd "" ) 'mc/mark-all-dwim)
;; (global-set-key (kbd "" ) 'mc/mark-all-like-this)
;; (global-set-key (kbd "" ) 'mc/mark-all-like-this-dwim)
;; (global-set-key (kbd "" ) 'mc/mark-all-like-this-in-defun)
;; (global-set-key (kbd "" ) 'mc/mark-all-symbols-like-this)
;; (global-set-key (kbd "" ) 'mc/mark-all-symbols-like-this-in-defun)
;; (global-set-key (kbd "" ) 'mc/mark-all-words-like-this)
;; (global-set-key (kbd "" ) 'mc/mark-all-words-like-this-in-defun)
;; (global-set-key (kbd "" ) 'mc/mark-more-like-this-extended)
;; (global-set-key (kbd "" ) 'mc/mark-next-like-this-word)
;; (global-set-key (kbd "" ) 'mc/mark-next-lines)
;; (global-set-key (kbd "" ) 'mc/mark-next-symbol-like-this)
;; (global-set-key (kbd "" ) 'mc/mark-next-word-like-this)
;; (global-set-key (kbd "" ) 'mc/mark-pop)
;; (global-set-key (kbd "" ) 'mc/mark-previous-like-this)
;; (global-set-key (kbd "" ) 'mc/mark-previous-lines)
;; (global-set-key (kbd "" ) 'mc/mark-previous-symbol-like-this)
;; (global-set-key (kbd "" ) 'mc/mark-previous-word-like-this)
;; (global-set-key (kbd "" ) 'mc/mark-sgml-tag-pair)
;; (global-set-key (kbd "" ) 'mc/reverse-regions)
;; (global-set-key (kbd "" ) 'mc/skip-to-next-like-this)
;; (global-set-key (kbd "" ) 'mc/skip-to-previous-like-this)
;; (global-set-key (kbd "" ) 'mc/sort-regions)
;; (global-set-key (kbd "" ) 'mc/toggle-cursor-on-click)
;; (global-set-key (kbd "" ) 'mc/vertical-align)
;; (global-set-key (kbd "" ) 'mc/vertical-align-with-space)

(require 'ob-tangle)
(require 'cl)
(require 'cl-lib)
(setq debug-on-error t)

(defun move-text-internal (arg)
  (cond
   ((and mark-active transient-mark-mode)
    (if (> (point) (mark))
        (exchange-point-and-mark))
    (let ((column (current-column))
          (text (delete-and-extract-region (point) (mark))))
      (forward-line arg)
      (move-to-column column t)
      (set-mark (point))
      (insert text)
      (exchange-point-and-mark)
      (setq deactivate-mark nil)))
   (t
    (let ((column (current-column)))
      (beginning-of-line)
      (when (or (> arg 0) (not (bobp)))
        (forward-line)
        (when (or (< arg 0) (not (eobp)))
          (transpose-lines arg)
          (when (and (eval-when-compile
                       '(and (>= emacs-major-version 24)
                             (>= emacs-minor-version 3)))
                     (< arg 0))
            (forward-line -1)))
        (forward-line -1))
      (move-to-column column t)))))

(defun move-text-down (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines down."
  (interactive "*p")
  (move-text-internal arg))

(defun move-text-up (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines up."
  (interactive "*p")
  (move-text-internal (- arg)))(defun move-text-internal (arg)
  (cond
   ((and mark-active transient-mark-mode)
    (if (> (point) (mark))
        (exchange-point-and-mark))
    (let ((column (current-column))
          (text (delete-and-extract-region (point) (mark))))
      (forward-line arg)
      (move-to-column column t)
      (set-mark (point))
      (insert text)
      (exchange-point-and-mark)
      (setq deactivate-mark nil)))
   (t
    (let ((column (current-column)))
      (beginning-of-line)
      (when (or (> arg 0) (not (bobp)))
        (forward-line)
        (when (or (< arg 0) (not (eobp)))
          (transpose-lines arg)
          (when (and (eval-when-compile
                       '(and (>= emacs-major-version 24)
                             (>= emacs-minor-version 3)))
                     (< arg 0))
            (forward-line -1)))
        (forward-line -1))
      (move-to-column column t)))))

(defun move-text-down (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines down."
  (interactive "*p")
  (move-text-internal arg))

(defun move-text-up (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines up."
  (interactive "*p")
  (move-text-internal (- arg)))


(global-set-key [M-up] 'move-text-up)

(global-set-key [M-down] 'move-text-down)

(defun join-line-below (arg)
  (interactive "p")
  (end-of-line)
  (delete-char 1)
  (delete-horizontal-space)
  (insert " "))

(defun join-region (beg end)
"Apply join-line over region."
    (interactive "r")
(if mark-active
    (let ((beg (region-beginning))
          (end (copy-marker (region-end))))
      (goto-char beg)
      (while (< (point) end)
        (join-line 1)))
))

;; Join below Line        
(global-set-key "\M-j" 'join-line-below)
(global-set-key (kbd "C-M-j") 'join-region)

(setq scroll-step 1 scroll-conservatively  10000)

;; set up unicode
(prefer-coding-system       'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)                      
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

;; use org-bullets-mode for utf8 symbols as org bullets
(require 'org-bullets)
;; make available "org-bullet-face" such that I can control the font size individually
(setq org-bullets-face-name (quote org-bullet-face))
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
(add-hook 'org-mode-hook (lambda () (org-indent-mode  1)))
(setq org-bullets-bullet-list '( "|" "║" "| " " " "✙" "♱" "♰" "☥" "✞" "✟" "✝" "†" "✠" "✚" "✜" "✛" "✢" "✣" "✤" "✥"))

(setq org-publish-project-alist
  '(("html"
     :base-directory "~/org/"
     :base-extension "org"
     :publishing-directory "~/org/html"
     :publishing-function org-html-publish-to-html )
    ("pdf"
     :base-directory "~/org/"
     :base-extension "org"
     :publishing-directory "~/org/pdf"
     :publishing-function org-latex-export-to-pdf )
    ("config"
     :base-directory "~/.emacs.d/"
     :base-extension "org"
     :publishing-directory "~/.emacs.d/"
     :publishing-function org-html-publish-to-html )
    ("all" :components ("html" "pdf" "config"))))

(defun org-show-current-heading-tidily ()
  (interactive)  ;Inteactive
  "Show next entry, keeping other entries closed."
  (if (save-excursion (end-of-line) (outline-invisible-p))
      (progn (org-show-entry) (show-children))
    (outline-back-to-heading)
    (unless (and (bolp) (org-on-heading-p))
      (org-up-heading-safe)
      (hide-subtree)
      (error "Boundary reached"))
    (org-overview)
    (org-reveal t)
    (org-show-entry)
    (show-children)))

(global-set-key "\M-=" 'org-show-current-heading-tidily)

(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)" "WAITING(w)" "HOLD(h)" "|" "CANCELLED(c)" "PHONE" "MEETING"))))

(setq org-todo-keyword-faces
      (quote (
              ("TODO"      :foreground "red"          :weight bold)
              ("NEXT"      :foreground "blue"         :weight bold)
              ("DONE"      :foreground "forest green" :weight bold)
              ("WAITING"   :foreground "orange"       :weight bold)
              ("HOLD"      :foreground "orange"       :weight bold)
              ("CANCELLED" :foreground "forest green" :weight bold)
              ("MEETING"   :foreground "forest green" :weight bold)
              ("PHONE"     :foreground "forest green" :weight bold)
             )))

;; set maximum indentation for description lists
(setq org-list-description-max-indent 1)

;; prevent demoting heading also shifting text inside sections
(setq org-adapt-indentation nil)

;; Do not treat underscore as subscript in org-mode
(setq org-export-with-sub-superscripts nil)

(setq org-src-fontify-natively t)

(defun org2html-replace-pre (html)
  "Replace pre blocks with sourcecode shortcode blocks.
shamelessly copied from org2blog/wp-replace-pre()"
  (save-excursion
    (let (pos code lang info params header code-start code-end html-attrs pre-class)
      (with-temp-buffer
        (insert html)
        (goto-char (point-min))
        (save-match-data
          (while (re-search-forward "<pre\\(.*?\\)>" nil t 1)

            ;; When the codeblock is a src_block
            (unless
                (save-match-data
                  (setq pre-class (match-string-no-properties 1))
                  (string-match "example" pre-class))
              ;; Replace the <pre...> text
              (setq lang (replace-regexp-in-string "src \\(.*\\)" "src linenums prettyprint nocode \\1" pre-class)  )

              (replace-match "")
              (setq code-start (point))

              ;; Go to end of code and remove </pre>
              (re-search-forward "</pre.*?>" nil t 1)
              (replace-match "")
              (setq code-end (point))
              (setq code (buffer-substring-no-properties code-start code-end))

              ;; Delete the code
              (delete-region code-start code-end)
              ;; Stripping out all the code highlighting done by htmlize
              ;; (setq code (replace-regexp-in-string "<.*?>" "" code))

              ;; default is highlight.js, it's the best!
              (insert (concat "\n<pre "
                              lang
                              "\" >\n"
                              code
                              "</pre>\n"))

              )))

        ;; Get the new html!
        (setq html (buffer-substring-no-properties (point-min) (point-max))))
      ))
  html)

(defun org2html-wrap-blocks-in-code (src backend info)
  (if (org-export-derived-backend-p backend 'html)
      (org2html-replace-pre src)))

(eval-after-load 'ox
  '(progn
     (add-to-list 'org-export-filter-src-block-functions
                  'org2html-wrap-blocks-in-code)
     ))

(org-babel-do-load-languages 'org-babel-load-languages '((python . t)))
(org-babel-do-load-languages 'org-babel-load-languages '((shell  . t)))

;; weather from wttr.in
(setq wttrin-default-cities '("Lombard"))
