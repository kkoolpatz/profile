%Title: Python 101 - The Data Types
%Author: Shantanu N Kulkarni <s@zsh.in>


Python data types are divided in 2 categories
- mutable (lists, dictionaries)
- immutable (integers, strings, tuples)

Mutable means can be changed _inplace_ where as immutable means that
these cannot be changed inplace.

---

# Integers (Immutable)

	>>> a=22
	>>> add10 = a+10
	>>> add10 # OUT: 32

	>>> a*10 # OUT: 220

	>>> pi = a/7
	>>> pi # OUT: 3
	>>> pi = a/7.0
	>>> pi # OUT: 3.142857142857143

	>>> round(pi,3) # OUT: 3.143

	>>> pow(3,3) # OUT: 27

_Exercise - Find out how to use max() and min()_

---

# Strings (Immutable)

	s = 'shantanu n kulkarni'

### Check membership
	>>> 'shan' in s # OUT: True
	>>> 'zhan' in s # OUT: False



### String Slices

	>>> s[0] # OUT: 's'
	>>> s[3] # OUT: 'n'
	>>> s[-1] # OUT: 'i'
	>>> s[-3] # OUT: 'r'

---

### String Slices 

	>>> s[1:5] # OUT: 'hant'
	>>> s[:3] # OUT: 'sha'
	>>> s[13:] # OUT: 'lkarni'
	>>> s[2:13:2] # OUT: 'atn  u'

	>>> s[::-1]
	# OUT: 'inrakluk n unatnahs'

---

### Swapping of two strings

	>>> s1 = 'spam'
	>>> s2 = 'eggs'
	>>> s1,s2 = s2, s1
	>>> s1, s2 # OUT: ('eggs', 'spam')

^
### Using + and * operators on lists
	>>> s1 + s2 # OUT: 'eggsspam'
	>>> s1 * 3  # OUT: 'eggseggseggs'

---


'Lists (mutable)'
# OUT: 'Lists (mutable)'

 	>>> x = ['foo', 'bar', 'car']

	>>> x[0] # OUT: 'foo'
	>>> x[0] = 'zoo'
	>>> x # OUT: ['zoo', 'bar', 'car']
	>>> x[0][0] # OUT: 'z'
	>>> x[0:1] # OUT: ['zoo']

'''Exercise: What is the difference between x[0] and x[0:1]'''

	>>> y = x
	>>> y[0] = 'moo'
	>>> y # OUT: ['moo', 'bar', 'car']
	>>> x # OUT: ['moo', 'bar', 'car']

### making a copy
	>>> z = x[:]
	>>> id(x), id(y), id(z) # OUT: (140419120018568, 140419120018568, 140419120019072)

'''Exercise: Change x[0] to doo and see how it affects y and z'''

### List expansion and compression'''

	>>> x[0:2] # OUT: ['moo', 'bar']
	>>> x[0:2] = ['lion','tiger','bear','wolf']
	>>> x # OUT: ['lion', 'tiger', 'bear', 'wolf', 'car']
	>>> x[0:4] # OUT: ['lion', 'tiger', 'bear', 'wolf']
	>>> x[0:4] = 'animals'
	>>> x # OUT: ['a', 'n', 'i', 'm', 'a', 'l', 's', 'car']
	>>> x=z
	>>> x # OUT: ['moo', 'bar', 'car']
	>>> x[0:2] = ['lion','tiger','bear','wolf']
	>>> x # OUT: ['lion', 'tiger', 'bear', 'wolf', 'car']
	>>> x[0:4] = ['animals']
	>>> x # OUT: ['animals', 'car']

_'Nested lists'_

	>>> x = [['one','two','three'], ['first','second']]
	>>> x # OUT: [['one', 'two', 'three'], ['first', 'second']]
	>>> x[0] # OUT: ['one', 'two', 'three']
	>>> x[0][1]
	>>> # OUT: 'two'
	>>> x[0][1][2] # OUT: 'o'
	>>> x = ['hi','hello' ]
	>>> x + x # OUT: ['hi', 'hello', 'hi', 'hello']
	>>> x * 2 # OUT: ['hi', 'hello', 'hi', 'hello']
	>>> x # OUT: ['hi', 'hello']
	>>> 'hi' in x # OUT: True
	>>> 'bye' in x # OUT: False
	>>> 'hi' in x[0] # OUT: True

### 'Tuples (immutable)'

	>>> t = ('hi', 'bye')
	>>> t[0] # OUT: 'hi'

_Exercise: Replace 'hi' with 'hello' in t_


### 'Dictionary (mutable)'

	>>> d = {'foo':'zoo', 'bar':'car'}
	>>> d # OUT: {'foo': 'zoo', 'bar': 'car'}
	>>> d['foo'] # OUT: 'zoo'

### Check membership

	>>> 'foo' in d # OUT: True
	>>> 'zoo' in d # OUT: False
	>>> d['new']='old'
	>>> d # OUT: {'new': 'old', 'foo': 'zoo', 'bar': 'car'}
	>>> x # OUT: ['hi', 'hello']
	>>> tuple(x) # OUT: ('hi', 'hello')
	>>> t # OUT: ('hi', 'bye')
	>>> list(t) # OUT: ['hi', 'bye']
